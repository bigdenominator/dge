The **Big Denominator** *DGE* serves as the backbone to all **Big Denominator** offerings by executing model libraries built with *[EDGE][]*, delivered by our customization Services, or licensed from our off-the-shelf applications. It is the application manager responsible for thread management, both intra-/inter-server communication, as well as configuration validation. Its simple and intuitive user interface facilitates administration of high performance applications.

The *DGE* offers ...

  * **intuitive** and **simple** interface for administration
  * **high performance** coordination of models and libraries
  * **low latency** / **high thoughput** data transfer
  * **parallel execution** through custom-administered multi-threading
  * **distributed computing** using the messaging / communication system of your choice

The *DGE* is offered as a stand-alone download for the convenience of IT departments; its power and utility comes from the models it runs. Feel free to [contact][] us for a trial license. 

----

# v0.9: 2016-08-05 #

Release 0.9 represents a major step in the history of **Big Denominator** - the initial deployment of the *DGE*. While a Win64 version is currently released, we anticipate release of a Linux engine shortly.

----

## + Contents ##

When installed on a Windows machine, the *DGE* creates a *BD_ROOT* environmental variable that references the installation directory. The following subdirectories and files are installed to this directory:

* License.pdf - Contains the terms of the **DGE** license.

----

**bin/**  *Subdirectory for binaries.*

Contains release versions of the following binaries:

* [tcmalloc][]
* dge.dll
* BigDenominator.exe

----

**lib/**  *Subdirectory for libraries.*

Contains release version of the [tcmalloc] library.

----

## + License ##

Use, modification and distribution are subject to the Big Denominator DGE License, Version 1.0.
(See accompanying file License.pdf or copy at http://www.bigdenominator.com/license-dge)

----

## + Copyright ##

� 2016 [Big Denominator][], LLC. All rights reserved.

[Big Denominator]: http://bigdenominator.com "Big Denominator"

[contact]: http://bigdenominator.com/contact/ "Contact Us"

[DGE]: http://bigdenominator.com/dge "Directed Graph Engine"

[EDGE]: http://bigdenominator.com/edge "EDGE"

[SIMD]: https://en.wikipedia.org/wiki/SIMD/ "single-instruction, multiple data"

[tcmalloc]: https://github.com/gperftools/gperftools/ "tcmalloc"