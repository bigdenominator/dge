#ifndef INCLUDE_H_CACHEDATA
#define INCLUDE_H_CACHEDATA

/* (C) Copyright Big Denominator, LLC 2016.
* Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
* (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "bdsdk.h"
#ifdef WINDOWS
#pragma once
#endif 

#include "TrnsprtQueue.h"

class CCachedData
{
public:
	typedef size_t size_type;

	bool pop(STransportPtr inp_sTrnsprtPtr)
	{ 
		inp_sTrnsprtPtr = msTransportPtr; return true;
	}
	bool push(STransportPtr inp_sTrnsprtPtr)
	{ 
		msTransportPtr = inp_sTrnsprtPtr; return true;
	}

	CCachedData(size_type& inp_iSize) { inp_iSize = 1; }
	~CCachedData(void) {}

private:
	STransportPtr	msTransportPtr;
};

#endif
