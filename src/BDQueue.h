#ifndef INCLUDE_H_TRNSPRTQUEUE
#define INCLUDE_H_TRNSPRTQUEUE

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
* (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "bdsdk.h"

#include <vector>

class CTransportQueue
{
public:
	typedef CTransportQueue		_Myt;
	typedef size_t				size_type;

	CTransportQueue(const size_type& inp_iSize) 
		:mvElements(convertSize(inp_iSize)), miSize(inp_iSize), miRead(0), miWrite(0), mbActive(true) {}

	~CTransportQueue(void)
	{
		while (miRead < miWrite)
		{
			size_type iIndx(miRead++);
			convertIndex(iIndx);
			STransport::DestroyInstance(mvElements[iIndx].msTransportPtr.load());
		}
	}

	inline int64_t count(void) const
	{
		return static_cast<int64_t>(miWrite) - static_cast<int64_t>(miRead);
	}
	
	inline STransportPtr front(void)
	{
		size_type iIndx(miRead);
		convertIndex(iIndx);

		while (!mvElements[iIndx].msTransportPtr.load() && mbActive) {}
		return (mbActive ? mvElements[iIndx].msTransportPtr.load() : nullptr);
	}

	inline STransportPtr pop(void)
	{
		size_type iIndx(miRead++); 
		convertIndex(iIndx);

		mvElements[iIndx].mcPop.Enter();
		while (!mvElements[iIndx].msTransportPtr.load() && mbActive) {}

		STransportPtr sTmpPtr(mbActive ? mvElements[iIndx].msTransportPtr.exchange(nullptr) : nullptr);
		mvElements[iIndx].mcPop.Exit();

		return sTmpPtr;
	}

	inline bool push(STransportPtr inp_Value)
	{
		size_type iIndx(miWrite++); 
		convertIndex(iIndx); 
		
		mvElements[iIndx].mcPush.Enter();
		while (mvElements[iIndx].msTransportPtr.load() && mbActive) {}
		mvElements[iIndx].msTransportPtr.store(inp_Value);
		mvElements[iIndx].mcPush.Exit();

		return mbActive;
	}

	inline void shutdown(void) { mbActive = false; }

	inline size_type size(void) { return miSize; }

private:
	struct SElement
	{
		atomic<STransportPtr>	msTransportPtr;
		BDGate					mcPop;
		BDGate					mcPush;

		SElement(void) :msTransportPtr(nullptr) {}
	};

	enum :size_type { CNT_PER_CACHELINE = 64 / sizeof(SElement) };

	vector<SElement>	mvElements;

	const size_type		miSize;
	atomic<size_type>	miRead;
	atomic<size_type>	miWrite;

	bool mbActive;

	inline void convertIndex(size_type& inp_iIndx) const
	{
		inp_iIndx %= miSize;
		inp_iIndx *= CNT_PER_CACHELINE;
	}

	static inline size_type convertSize(size_type inp_iSize)
	{
		return inp_iSize * CNT_PER_CACHELINE;
	}
};
#endif