#ifndef INCLUDE_H_DGELIB
#define INCLUDE_H_DGELIB

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
* (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"
#ifdef WINDOWS
#include <SDKDDKVer.h>
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#include <windows.h>					// Windows Header Files:
#ifdef DGELIB_EXPORTS
#define DGELIB_API __declspec(dllexport)
#else
#define DGELIB_API __declspec(dllimport)
#endif
#else
#ifdef DGELIB_EXPORTS
#define DGELIB_API __attribute__ ((visibility("default")))
#else
#define DGELIB_API
#endif
#endif

extern "C" DGELIB_API int ExecuteGraphs(int argc, char* argv[]);

#endif
