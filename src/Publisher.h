#ifndef INCLUDE_H_PUBLISHER
#define INCLUDE_H_PUBLISHER

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
* (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "bdsdk.h"

#include "security/all.h"
#include "FileHndlr.h"
#include "SQLiteHndlr.h"

PUBLISH(1, (), (CFlatFile, CSQLite));

static int BootstrapDge(SLibBootstrapRwPtr inp_sModelLibListPtr)
{
	const string sLicenseKey = "0HnuW2q5/E+feEQcOivYF/+TWuPaLDu9JKg18+53WN8=";

	SLicensePtr sLicensePtr(CreateSmart<SLicense>());
	switch (ValidateLicense(sLicenseKey, inp_sModelLibListPtr->msLicense, LIBRARY::ID, *sLicensePtr))
	{
	case LICENSE::VALID:
		mcEvntHndlrIntrPtr = inp_sModelLibListPtr->mcEvntHndlrPtr;
		LibraryEnv::NodeSpecLstHlpr<PublishedLibNodeLst>::add(inp_sModelLibListPtr->mcNodeSpecLstPtr);
		LibraryEnv::SchemaFnLstHlpr<PublishedLibNodeLst>::add(inp_sModelLibListPtr->mcSchemaFnLstPtr);
		LibraryEnv::TechSpecLstHlpr<PublishedLibTechLst>::add(inp_sModelLibListPtr->mcTechSpecLstPtr);
		return 0;
		break;
	default:
		return 1;
		break;
	}
}
#endif