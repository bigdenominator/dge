#ifndef INCLUDE_H_BDGRAPH
#define INCLUDE_H_BDGRAPH

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
* (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "bdsdk.h"

#include <thread>
#include "EvntHndlr.h"
#include "ConnHndlr.h"
#include "LibHndlr.h"
#include "Publisher.h"

static const string sBadLibOpen = "Unable to open library: ";
static const string sBadLibEntry = "Unable to find entry point in library: ";
static const string sBadLibBootstrap = "Unable to bootstrap library: ";
static const string sNoLibForTransformNode = "No library loaded defines node: ";
static const string sNoTransformNodes = "No nodes to launch";
static const string sNoInputs = "No input external connections";
static const string sNoOutputs = "No output external connections";
static const string sUnidentifiedInputSchematic = "Unidentified input schematic: ";
static const string sUnidentifiedTech = "Unidentified technology: ";
static const string sUnsatisfiedRequest = "No node is defined to satisfy request: ";
static const string sUnsatisfiedSend = "No node or output connection is defined to satisfy send: ";
static const string sUnsatisfiedView = "No node or input connection is defined to satisfy view: ";


typedef CSmartMap<uint32_t, SLibPktPtr>	CLibLst;
typedef CSmartMap<TYPELIST_t, uint32_t>	CNodeCntLst;
typedef CSmartVector<SConnSpecPtr>	CConnSpecLst;

typedef CSmartPtr<CLibLst> CLibLstPtr;
typedef CSmartPtr<CNodeCntLst> CNodeCntLstPtr;
typedef CSmartPtr<CConnSpecLst> CConnSpecLstPtr;

SMARTSTRUCT(SGraphDef, (string msLicense; CLibLstPtr mcLibLstPtr; CNodeCntLstPtr mcNodeCntLstPtr; CConnSpecLstPtr mcConnSpecLstPtr;), (msLicense, mcLibLstPtr, mcNodeCntLstPtr, mcConnSpecLstPtr))

class CGraph
{
public:
	CGraph(SGraphDefPtr inp_sGraphDefPtr) :msGraphDefPtr(inp_sGraphDefPtr)
	{
		mcEvntHndlrPtr = CreateUnique<CEvntHndlr>();
		mcThreadKeyPtr = CreateSmart<CThreadKey>();
		msBootstrapPtr = CreateSmart<SLibBootstrap>();

		mcEvntHndlrIntrPtr = mcEvntHndlrPtr.get();

		if (!loadLibraries(inp_sGraphDefPtr->msLicense, inp_sGraphDefPtr->mcLibLstPtr)) return;
		if (!buildThreadKey(inp_sGraphDefPtr->mcNodeCntLstPtr)) return;
		buildEventCalls();
		addStandardExternals(inp_sGraphDefPtr->mcConnSpecLstPtr);
		if (!buildExternals(inp_sGraphDefPtr->mcConnSpecLstPtr)) return;
		if (!validateConfig()) return;
	}

	~CGraph(void)
	{
		mcEvntHndlrPtr.reset(); 
		msBootstrapPtr.reset(); 
		mcThreadKeyPtr.reset();

		auto itr(iterators::GetItr(msGraphDefPtr->mcLibLstPtr));
		LOOP(itr)
		{
			LibHandler::close(*itr->second);
		}
	}

	void Execute(void)
	{
		CThreadLst cThreadLst;
		CThreadLst cInputNodeThreadLst;

		mcEvntHndlrPtr->configure(mcCalls.mcAll);

		FOREACH((*mcThreadKeyPtr), itr)
		{
			CNodePtr cThisNodePtr(itr->second.second);
			for (uint32_t iCnt(0); iCnt < itr->second.first; iCnt++)
			{
				cThreadLst.emplace_back(std::thread(&CBaseNode::Run, cThisNodePtr));
			}
		}

		FOREACH(mcInputs, itr)
		{
			if (mcThreadKeyPtr->find(itr->first) != mcThreadKeyPtr->end())
			{
				cInputNodeThreadLst.emplace_back(std::thread(&CBaseNode::Run, itr->second)); 
			}
			else
			{
				cThreadLst.emplace_back(std::thread(&CBaseNode::Run, itr->second));
			}
		}

		FOREACH(mcOutputs, itr)
		{
			cThreadLst.emplace_back(std::thread(&CBaseNode::Run, itr->second));
		}

		mcEvntHndlrPtr->release();
		shutdownGate1(cInputNodeThreadLst);
		shutdownGate2();
		mcEvntHndlrPtr->shutdown();
		for (std::thread& cThread : cThreadLst)
		{
			cThread.join();
		}
	}

	bool IsValid(void) { return mcFeedback; }

	CFeedback& GetFeedback(void) { return mcFeedback; }

protected:
	typedef pair<uint32_t, CNodePtr>		LaunchPair;
	typedef CSimpleMap<TYPELIST_t, LaunchPair>	CThreadKey;
	typedef CSimpleMap<TYPELIST_t, CNodePtr>	CXtrnKey;

	SIMPLESTRUCT(EvntCalls, (CTypeLst mcRequests; CTypeLst mcSends; CTypeLst mcViews; CTypeLst mcAll;));

	typedef vector<std::thread>	CThreadLst;

	enum :uint32_t { LIBCODE = 1000 };

	SGraphDefPtr msGraphDefPtr;

	CUniquePtr<CEvntHndlr>	mcEvntHndlrPtr;
	CSmartPtr<CThreadKey>	mcThreadKeyPtr;
	SLibBootstrapPtr		msBootstrapPtr;

	EvntCalls				mcCalls;
	
	CXtrnKey				mcInputs;
	CXtrnKey				mcOutputs;

	CFeedback				mcFeedback;

	bool allOnReceive(void)
	{
		FOREACH((*mcThreadKeyPtr), itr)
		{
			if (mcEvntHndlrPtr->count(itr->first) + itr->second.first > 0) { return false; }
		}
		
		FOREACH(mcOutputs, itr)
		{
			if (mcEvntHndlrPtr->count(itr->first) > 0) { return false; }
		}

		return true;
	}

	static void addStandardExternals(CConnSpecLstPtr inp_cConnSpecListPtr)
	{
		SConnSpecPtr sConn1Ptr(CreateSmart<SConnSpec>());
		sConn1Ptr->mTechID = CFlatFile::TechId;
		sConn1Ptr->msConnectionString = "./bderr.log";
		sConn1Ptr->mcActivitiesPtr = CreateSmart<CActivitySpecLst>();

		SActivitySpecPtr sSpec1Ptr(CreateSmart<SActivitySpec>());
		sSpec1Ptr->mDirection = IO_DIR::OUTPUT;
		sSpec1Ptr->mFormat = SUMMARY_FORMAT::SERIALIZED;
		sSpec1Ptr->msActivityString = "error";
		sConn1Ptr->mcActivitiesPtr->add(SError::TypeId, sSpec1Ptr);

		SConnSpecPtr sConn2Ptr(CreateSmart<SConnSpec>());
		sConn2Ptr->mTechID = CFlatFile::TechId;
		sConn2Ptr->msConnectionString = "./bdlog.log";
		sConn2Ptr->mcActivitiesPtr = CreateSmart<CActivitySpecLst>();

		SActivitySpecPtr sSpec2Ptr(CreateSmart<SActivitySpec>());
		sSpec2Ptr->mDirection = IO_DIR::OUTPUT;
		sSpec2Ptr->mFormat = SUMMARY_FORMAT::SERIALIZED;
		sSpec2Ptr->msActivityString = "log";
		sConn2Ptr->mcActivitiesPtr->add(SLog::TypeId, sSpec2Ptr);
		
		inp_cConnSpecListPtr->push_back(sConn1Ptr);
		inp_cConnSpecListPtr->push_back(sConn2Ptr);
	}

	bool buildExternals(CConnSpecLstPtr inp_cConnSpecListPtr)
	{
		auto itrTech(iterators::GetConstItr(msBootstrapPtr->mcTechSpecLstPtr));
		auto itrConn(iterators::GetConstItr(inp_cConnSpecListPtr));

		LOOP(itrConn)
		{
			const TECHNOLOGY_t iTech(itrConn.value()->mTechID);
			if (!itrTech.find(iTech))
			{
				FEEDBACK(mcFeedback, 101, sUnidentifiedTech + to_string(iTech));
				return false;
			}

			CConnHndlrPtr cConnPtr(CreateSmart<CConnHndlr>(itrTech.value()));
			if (!cConnPtr->Open(itrConn.value()->msConnectionString))
			{
				mcFeedback = cConnPtr->GetFeedback();
				return false;
			}

			auto itrFnLst(iterators::GetConstItr(msBootstrapPtr->mcSchemaFnLstPtr));
			auto itrActs(iterators::GetConstItr(itrConn.value()->mcActivitiesPtr));
			LOOP(itrActs)
			{
				SActivityPtr sActPtr(cConnPtr->Configure(itrActs.value()->msActivityString, itrActs.value()->mDirection, itrActs.value()->mFormat));
				if (!sActPtr)
				{
					mcFeedback = cConnPtr->GetFeedback();
					return false;
				}

				const TYPELIST_t iType(itrActs.key());
				switch (itrActs.value()->mDirection)
				{
				case IO_DIR::INPUT:
					if (!itrFnLst.find(iType))
					{
						FEEDBACK(mcFeedback, 112, sUnidentifiedInputSchematic + to_string(iType))
						return false;
					}
					mcInputs.add(iType, CreateSmart<CInputNode>(cConnPtr, sActPtr, itrFnLst.value()));
					break;
				default:
					mcOutputs.add(iType, CreateSmart<COutputNode>(cConnPtr, sActPtr, iType));
					break;
				}
			}
		}

		return true;
	}

	void buildEventCalls(void)
	{
		auto itrNodes(iterators::GetConstItr(msBootstrapPtr->mcNodeSpecLstPtr));
		auto itrThreads(iterators::GetConstItr(mcThreadKeyPtr));

		LOOP(itrThreads)
		{
			const TYPELIST_t iType(itrThreads.key());
			itrNodes.find(iType);

			mcCalls.mcAll.insert(iType);

			auto itrRequests(iterators::GetConstItr(itrNodes.value()->mcRequestsPtr));
			LOOP(itrRequests)
			{
				mcCalls.mcRequests.insert(itrRequests.value());
				mcCalls.mcAll.insert(itrRequests.value());
			}

			auto itrSends(iterators::GetConstItr(itrNodes.value()->mcSendsPtr));
			LOOP(itrSends)
			{
				mcCalls.mcSends.insert(itrSends.value());
				mcCalls.mcAll.insert(itrSends.value());
			}

			auto itrViews(iterators::GetConstItr(itrNodes.value()->mcViewsPtr));
			LOOP(itrViews)
			{
				mcCalls.mcViews.insert(itrViews.value());
				mcCalls.mcAll.insert(itrViews.value());
			}
		}
	}

	bool buildThreadKey(CNodeCntLstPtr inp_cNodeCntLstPtr)
	{
		auto itrNodeCnt(iterators::GetConstItr(inp_cNodeCntLstPtr));
		auto itrNodeSpecLst(iterators::GetConstItr(msBootstrapPtr->mcNodeSpecLstPtr));

		LOOP(itrNodeCnt)
		{
			if (itrNodeCnt.value() > 0)
			{
				const TYPELIST_t iType(itrNodeCnt.key());
				if (itrNodeSpecLst.find(iType))
				{
					mcThreadKeyPtr->add(iType, LaunchPair(itrNodeCnt.value(), itrNodeSpecLst.value()->mfNodeFactory()));
				}
				else
				{
					FEEDBACK(mcFeedback, 102, sNoLibForTransformNode + to_string(iType));
					break;
				}
			}
		}

		return mcFeedback;
	}

	bool loadLibraries(const string& inp_sLicense, CLibLstPtr inp_cLibListPtr)
	{
		msBootstrapPtr->msLicense = inp_sLicense;
		msBootstrapPtr->mcEvntHndlrPtr = mcEvntHndlrIntrPtr;
		msBootstrapPtr->mcNodeSpecLstPtr = CreateSmart<CNodeSpecLst>();
		msBootstrapPtr->mcTechSpecLstPtr = CreateSmart<CTechSpecLst>();
		msBootstrapPtr->mcSchemaFnLstPtr = CreateSmart<CSchemaFnLst>();

		if (BootstrapDge(msBootstrapPtr.get()) != 0)
		{
			FEEDBACK(mcFeedback, 105, sBadLibBootstrap + "DGE");
			return false;
		}

		auto itr(iterators::GetConstItr(inp_cLibListPtr));
		LOOP(itr)
		{
			msBootstrapPtr->msLicense = itr.value()->msLicense;
			if (!loadLibrary(*itr.value())) return false;
		}

		return true;
	}

	bool loadLibrary(SLibPkt& inp_sLib)
	{
		if (inp_sLib.msPath.back()!= cSLASH)
		{
			inp_sLib.msPath += cSLASH;
		}

		if (!LibHandler::open(inp_sLib))
		{
			FEEDBACK(mcFeedback, 103, sBadLibOpen + inp_sLib.msName);
			return false;
		}
		if (!LibHandler::findEntry(inp_sLib))
		{
			FEEDBACK(mcFeedback, 104, sBadLibEntry + inp_sLib.msName);
			return false;
		}
		if (inp_sLib.mfEntry(msBootstrapPtr.get()) != 0)
		{
			FEEDBACK(mcFeedback, 105, sBadLibBootstrap + inp_sLib.msName);
			return false;
		}

		return true;
	}

	static void shutdownGate1(CThreadLst& inp_cThreads)
	{
		for (std::thread& cThread : inp_cThreads)
		{
			cThread.join();
		}
	}

	void shutdownGate2(void)
	{
		bool bAllOnReceive(false);
		while (!bAllOnReceive)
		{
			while (!allOnReceive()) {}
			bAllOnReceive = allOnReceive();
		}
	}

	bool validateConfig(void)
	{
		if (mcThreadKeyPtr->empty())
		{
			FEEDBACK(mcFeedback, 106, sNoTransformNodes);
			return false;
		}
		if (mcInputs.empty())
		{
			FEEDBACK(mcFeedback, 107, sNoInputs);
			return false;
		}
		if (mcOutputs.empty())
		{
			FEEDBACK(mcFeedback, 108, sNoOutputs);
			return false;
		}

		if (!validateRequests()) return false;
		if (!validateSends()) return false;
		if (!validateViews()) return false;

		return true;
	}

	bool validateRequests(void)
	{
		auto itrNodes(iterators::GetConstItr(mcThreadKeyPtr));

		FOREACH(mcCalls.mcRequests, itr)
		{
			const TYPELIST_t iType(*itr); 
			if (!itrNodes.find(iType))
			{
				FEEDBACK(mcFeedback, 109, sUnsatisfiedRequest + to_string(iType));
				return false;
			}
		}

		return true;
	}

	bool validateSends(void)
	{
		auto itrNodes(iterators::GetConstItr(mcThreadKeyPtr));

		FOREACH(mcCalls.mcSends, itr)
		{
			const TYPELIST_t iType(*itr);
			if (!itrNodes.find(iType))
			{
				if (mcOutputs.find(iType) == mcOutputs.end())
				{
					if (mcCalls.mcViews.find(iType) == mcCalls.mcViews.end())
					{
						FEEDBACK(mcFeedback, 110, sUnsatisfiedSend + to_string(iType));
						return false;
					}
				}
			}
		}

		return true;
	}

	bool validateViews(void)
	{
		FOREACH(mcCalls.mcViews, itr)
		{
			const TYPELIST_t iType(*itr);
			if (mcInputs.find(iType) == mcInputs.end())
			{
				if (mcCalls.mcSends.find(iType) == mcCalls.mcSends.end())
				{
					FEEDBACK(mcFeedback, 111, sUnsatisfiedView + to_string(iType));
					return false;
				}
			}
		}

		return true;
	}
};
#endif
