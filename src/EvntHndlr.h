#ifndef INCLUDE_H_EVNTHNDLR
#define INCLUDE_H_EVNTHNDLR

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
* (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "bdsdk.h"

#include <vector>
#include "BDQueue.h"

class CEvntHndlr : public IEvntHndlr
{
public:
	CEvntHndlr(void)
	{
		mbShutdown = false;
		miTransactionID = 0;
	}

	~CEvntHndlr(void) {}

	inline void Attach(void)
	{
		mcWait.WaitForRelease();
	}

	inline STransportPtr GetTransport(void)
	{
		STransportPtr sTempPtr(mcPoolPtr->pop());
		sTempPtr->initialize();
		sTempPtr->miTransID = getTransactionID();

		return sTempPtr;
	}

	inline STransportPtr Receive(TYPELIST_t inp_iType) const
	{
		return mcQueueLst[inp_iType]->pop();
	}

	inline bool Request(STransportPtr inp_cTransportPtr) const
	{
		if (mcQueueLst[inp_cTransportPtr->msDataHldrPtr->GetTypeId()]->push(inp_cTransportPtr))
		{
			while (inp_cTransportPtr->msReturnInfo.mbRequestWaiting) {}
			return true;
		}

		return false;
	}

	inline void ReturnTransport(STransportPtr inp_sTrnsprtPtr) const
	{
		mcPoolPtr->push(inp_sTrnsprtPtr);
	}

	inline bool Send(STransportPtr inp_cTransportPtr) const 
	{
		return mcQueueLst[inp_cTransportPtr->msDataHldrPtr->GetTypeId()]->push(inp_cTransportPtr);
	}

	inline STransportPtr View(TYPELIST_t inp_iType) const
	{
		return mcQueueLst[inp_iType]->front();
	}

	inline void configure(const CTypeLst& inp_cTypes)
	{
		mcQueueLst.resize(inp_cTypes.back() + 1);

		queue_type::size_type iCapacity(0);
		addQueues(inp_cTypes, iCapacity);
		buildPool(iCapacity);
	}

	inline int64_t count(TYPELIST_t inp_iType) const
	{
		return mcQueueLst[inp_iType]->count();
	}

	inline void release(void)
	{
		mcWait.Release();
	}

	inline void shutdown(void) const
	{
		for (CQueueLst::size_type iQueue(0); iQueue < mcQueueLst.size(); iQueue++)
		{
			if (mcQueueLst[iQueue])
			{
				mcQueueLst[iQueue]->shutdown();
			}
		}
	}

private:
	typedef CTransportQueue			queue_type;
	typedef CUniquePtr<queue_type>	queue_pointer;
	typedef vector<queue_pointer>	CQueueLst;

	typedef CTransportQueue			pool_type;
	typedef CUniquePtr<pool_type>	pool_pointer;

	enum :size_t { DEFAULT_SIZE = 128, POOL_SIZE_FACTOR = 2 };

	pool_pointer		mcPoolPtr;
	CQueueLst			mcQueueLst;
	
	bool				mbShutdown;
	atomic<uint64_t>	miTransactionID;
	BDWaitRelease       mcWait;

	template<typename TQueue> inline void addQueue(TYPELIST_t inp_iType, queue_type::size_type inp_iSize)
	{
		mcQueueLst[inp_iType] = CreateUnique<TQueue>(inp_iSize);
	}

	inline void addQueues(const CTypeLst& inp_cTypes, queue_type::size_type& inp_iTransportCapacity)
	{
		FOREACH(inp_cTypes, itrNodes)
		{
			addQueue<queue_type>(*itrNodes, DEFAULT_SIZE); 
			inp_iTransportCapacity += DEFAULT_SIZE;
		}
	}

	inline void buildPool(pool_type::size_type inp_iTotalQueueSize)
	{
		const pool_type::size_type iSize(inp_iTotalQueueSize * POOL_SIZE_FACTOR);

		mcPoolPtr = CreateUnique<pool_type>(iSize);
		for (uint64_t i(0); i < iSize; i++)
		{
			mcPoolPtr->push(STransport::CreateInstance());
		}
	}

	inline uint64_t getTransactionID(void) { return miTransactionID++; }
};
#endif
