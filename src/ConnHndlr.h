#ifndef INCLUDE_H_GRAPHEXTERN
#define INCLUDE_H_GRAPHEXTERN

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
* (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "bdsdk.h"

#include <atomic>

static const string sOutputNodeFailure = "Output node error for type ";

SMARTSTRUCT(SActivitySpec, (IO_DIR mDirection; SUMMARY_FORMAT mFormat; string msActivityString;), (mDirection, mFormat, msActivityString))
typedef CSmartMap<TYPELIST_t, SActivitySpecPtr>	CActivitySpecLst;
typedef CSmartPtr<CActivitySpecLst> CActivitySpecLstPtr;
SMARTSTRUCT(SConnSpec, (TECHNOLOGY_t mTechID;  string msConnectionString; CActivitySpecLstPtr mcActivitiesPtr;), (mTechID, msConnectionString, mcActivitiesPtr));


class CConnHndlr;
typedef CSmartPtr<CConnHndlr> CConnHndlrPtr;
class CConnHndlr
{
public:
	CConnHndlr(fTechFactory inp_fTechnology) :miActivities(0), mbOpen(false)
	{
		mcTechPtr = inp_fTechnology();
	}

	~CConnHndlr() {}

	inline void Close(void)
	{
		size_type iOpens(1);

		if (miActivities.compare_exchange_strong(iOpens, 0))
		{
			if (mbOpen)
			{
				mcTechPtr->Close();
				mbOpen = false;
			}
		}
		else miActivities--;
	}

	inline SActivityPtr Configure(const string& inp_sActivityString, IO_DIR inp_Direction, SUMMARY_FORMAT inp_Format)
	{
		SActivityPtr sActivityPtr(mbOpen ? mcTechPtr->Configure(inp_sActivityString, inp_Direction, inp_Format) : SActivityPtr());
		if (sActivityPtr) { miActivities++; }

		return sActivityPtr;
	}

	inline CFeedback& GetFeedback(void) { return mcTechPtr->mcFeedback; }

	inline bool IsOpen(void) const { return mbOpen; }

	inline bool Open(const string& inp_sConnectionString)
	{
		return mbOpen = mcTechPtr->Open(inp_sConnectionString);
	}

	inline bool Receive(SActivity& inp_cBuffer)
	{
		return mcTechPtr->Receive(inp_cBuffer);
	}

	inline bool Send(SActivity& inp_cBuffer)
	{
		return mcTechPtr->Send(inp_cBuffer);
	}

private:
	typedef int32_t	size_type;

	atomic<bool>		mbOpen;
	atomic<size_type>	miActivities;
	CTechPtr			mcTechPtr;
};

class CInputNode : public CBaseNode
{
public:
	typedef CBaseNode	_Mybase;
	typedef CInputNode	_Myt;

	CInputNode(CConnHndlrPtr inp_cConnPtr, SActivityPtr inp_sActivityPtr, fnSchematic inp_fSchematic)
		:_Mybase(), mcConnPtr(inp_cConnPtr), msActivityPtr(inp_sActivityPtr), mfSchematic(inp_fSchematic) {}

	size_t Run(void)
	{
		size_t lCount(0);

		mcEvntHndlrLocalPtr->Attach();

		if (Initialize())
		{
			bool bMore(true);
			while (bMore)
			{
				STransportPtr sTransportPtr(mcEvntHndlrLocalPtr->GetTransport());
				msActivityPtr->mcContainerPtr = mfSchematic(sTransportPtr->msDataHldrPtr);
				if (mcConnPtr->Receive(*msActivityPtr))
				{
					mcEvntHndlrLocalPtr->Send(sTransportPtr);
					lCount++;
				}
				else
				{
					mcEvntHndlrLocalPtr->ReturnTransport(sTransportPtr);
					bMore = false;
				}
			}
			mcConnPtr->Close();
		}
		else
		{
			SErrorPtr sErrPtr(CreateSmart<SError>());
			sErrPtr->mcFeedback = msActivityPtr->mcFeedback;

			STransportPtr cTempPtr(mcEvntHndlrLocalPtr->GetTransport());
			cTempPtr->msDataHldrPtr = CreateUnique<SGreyBox<SError>>(sErrPtr);

			mcEvntHndlrLocalPtr->Send(cTempPtr);
		}

		return lCount;
	}

protected:
	CConnHndlrPtr	mcConnPtr;
	SActivityPtr	msActivityPtr;

	fnSchematic		mfSchematic;
};

class COutputNode : public CBaseNode
{
public:
	typedef CBaseNode	_Mybase;
	typedef COutputNode	_Myt;

	COutputNode(CConnHndlrPtr inp_cConnPtr, SActivityPtr inp_sActivityPtr, TYPELIST_t inp_iType)
		:_Mybase(), mcConnPtr(inp_cConnPtr), msActivityPtr(inp_sActivityPtr), miType(inp_iType),
		msErrorPrefix(sOutputNodeFailure + to_string(inp_iType) + sColon + sSpace) {}

	size_t Run(void)
	{
		size_t lCount(0);

		mcEvntHndlrLocalPtr->Attach();

		if (Initialize())
		{
			STransportPtr sTmpPtr;
			while (sTmpPtr = mcEvntHndlrLocalPtr->Receive(miType))
			{
				msActivityPtr->mcContainerPtr = SGreyBox<CContainer<0>>::Extract(sTmpPtr->msDataHldrPtr).get();
				if (mcConnPtr->Send(*msActivityPtr)) { lCount++; }
				else GetFeedback() = msActivityPtr->mcFeedback;

				mcEvntHndlrLocalPtr->ReturnTransport(sTmpPtr);
			}
			mcConnPtr->Close();
		}
		else
		{
			SErrorPtr sErrPtr(CreateSmart<SError>());
			sErrPtr->mcFeedback = msActivityPtr->mcFeedback;

			STransportPtr cTempPtr(mcEvntHndlrLocalPtr->GetTransport());
			cTempPtr->msDataHldrPtr = CreateUnique<SGreyBox<SError>>(sErrPtr);

			mcEvntHndlrLocalPtr->Send(cTempPtr);
		}

		return lCount;
	}

protected:
	const string			msErrorPrefix;
	const TYPELIST_t		miType;

	CConnHndlrPtr		mcConnPtr;
	SActivityPtr		msActivityPtr;
};
#endif