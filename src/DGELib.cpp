
/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
* (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include <iostream>
#include "DGELib.h"
#include "BDGraphEx.h"

#ifdef WINDOWS
BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}
#endif

extern "C" DGELIB_API int ExecuteGraphs(int argc, char* argv[])
{
	CUniquePtr<CGraphEx> cGrphExPtr(CreateUnique<CGraphEx>(argc, argv));
	CFeedback cFeedback(cGrphExPtr->Execute());

	if (!cFeedback) std::cout << cFeedback.msInfo << endl;

	return cFeedback.miNum;
}
