#ifndef INCLUDE_H_GRAPHLIB
#define INCLUDE_H_GRAPHLIB

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
* (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "bdsdk.h"

#ifdef UNIX
#include <dlfcn.h>
#endif

#ifdef WINDOWS
SMARTSTRUCT(SLibPkt, (string msPath; string msName; string msEntry; string msLicense; HMODULE mcHandle; fLibEntry mfEntry;), (msPath, msName, msEntry, msLicense))

struct LibHandler
{
	static void close(SLibPkt& inp_sPkt)
	{
		if (inp_sPkt.mcHandle != 0x00) FreeLibrary(inp_sPkt.mcHandle);
	}

	static bool findEntry(SLibPkt& inp_sPkt)
	{
		inp_sPkt.mfEntry = (fLibEntry)GetProcAddress(inp_sPkt.mcHandle, inp_sPkt.msEntry.c_str());

		return inp_sPkt.mfEntry;
	}

	static bool open(SLibPkt& inp_sPkt)
	{
		const string	sFullName(inp_sPkt.msPath + inp_sPkt.msName);
		const char*		cFullNamePtr((char*)(sFullName.c_str()));
		const int		iNumChars(lstrlenA(cFullNamePtr) + 1);
		wchar_t*		wcBufPtr(new wchar_t[iNumChars]);

		MultiByteToWideChar(CP_ACP, 0, cFullNamePtr, iNumChars, wcBufPtr, iNumChars);
		std::wstring    wcLibName(wcBufPtr);
		delete[] wcBufPtr;
		LPCWSTR sLibraryName = wcLibName.c_str();
		HANDLE cLibHndl(0x00);
		DWORD           dwFlags(0);
		inp_sPkt.mcHandle = 0x00;
		inp_sPkt.mcHandle = LoadLibraryExW(sLibraryName, cLibHndl, dwFlags);

		return (inp_sPkt.mcHandle != 0x00);
	}
};
#else
SMARTSTRUCT(SLibPkt, (string msPath; string msName; string msEntry; string msLicense; void* mcHandle; fLibEntry mfEntry;), (msPath, msName, msEntry, msLicense))

struct LibHandler
{
	static void close(SLibPkt& inp_sPkt)
	{
		if (inp_sPkt.mcHandle != nullptr)
		{
			 dlclose(inp_sPkt.mcHandle);
		}
	}

	static bool findEntry(SLibPkt& inp_sPkt)
	{
		inp_sPkt.mfEntry = (fLibEntry)dlsym(inp_sPkt.mcHandle, inp_sPkt.msEntry.c_str());
	
		return inp_sPkt.mfEntry;
	}

	static bool open(SLibPkt& inp_sPkt)
	{
		string sFullName(inp_sPkt.msPath + inp_sPkt.msName);

		inp_sPkt.mcHandle = dlopen(sFullName.c_str(), RTLD_NOW | RTLD_DEEPBIND);
		
		return (inp_sPkt.mcHandle != nullptr);
	}
};
#endif
#endif
