ifndef SETUP_DGE
SETUP_DGE		= TRUE

include ../../utilities/Make/common.mk
include $(BDSDK_ROOT)/bdsdk.mk

DGE_DIR			= $(BDSDK_ROOT)/DGE
INCLUDE_DIRS		+= -I$(DGE_DIR)

DGE_LIB_REL		:= $(BDSDK_BIN_REL)/libdge.so
DGE_LIB_DBG		:= $(BDSDK_BIN_DBG)/libdge.so

DGE_VER_MAJOR		= 0
DGE_VER_MINOR		= 9
DGE_VER_RELEASE		= 1

DGE_REF_REL		= $(DGE_LIB_REL)
DGE_LINK_REL		= $(DGE_REF_REL).$(DGE_VER_MAJOR).$(DGE_VER_MINOR)

DGE_REF_DBG		= $(DGE_LIB_DBG)
DGE_LINK_DBG		= $(DGE_REF_DBG).$(DGE_VER_MAJOR).$(DGE_VER_MINOR)

CFLAGS_REL		+= -ldl -lpthread
CFLAGS_DBG		+= -ldl -lpthread

LFLAGS_REL		+= $(BDSDK_BIN_REL)/libcryptopp.a -lc -L$(BDSDK_BIN_REL) -L$(SQLITE_ROOT)/$(BUILD_REL) -lsqlite3
LFLAGS_DBG		+= $(BDSDK_BIN_DBG)/libcryptopp.a -lc -L$(BDSDK_BIN_DBG) -L$(SQLITE_ROOT)/$(BUILD_DBG) -lsqlite3
endif
