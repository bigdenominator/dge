#ifndef INCLUDE_H_GRAPHEX
#define INCLUDE_H_GRAPHEX

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
* (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "bdsdk.h"

#include "BDGraph.h"

static const string sDFLT_PATH = "/.";
static const string sDFLT_NAME = "bootstrap.db";
static const string sBOOTSTRAP_COMMAND = "SELECT * FROM vw_RunDef";

static const string sBadBootstrapConfig = "Unable to configure bootstrap read: ";
static const string sBadBootstrapOpen = "Unable to open bootstrap database: ";
static const string sBadBootstrapRead = "Unable to read bootstrap database: ";
static const string sBadBootstrapDeserialize = "Unable to deserialize graph: ";
static const string sBadGraphConfig = "Unable to configure graph: ";


class CGraphEx
{
public:
	CGraphEx(const string& inp_sPath = sDFLT_PATH, const string& inp_sName = sDFLT_NAME) :msBootPath(inp_sPath), msBootFile(inp_sName)
	{
		init();
	}

	CGraphEx(uint32_t inp_iArgCnt, char** inp_cList) :msBootPath(sDFLT_PATH), msBootFile(sDFLT_NAME)
	{
		parseCmdLineArgs(inp_iArgCnt, inp_cList);
		init();
	}

	~CGraphEx(void){}

	CFeedback Execute(void)
	{
		if (readBootstrap())
		{
			FOREACH((*mcGraphDefPtr), itr)
			{
				CUniquePtr<CGraph> cGraphPtr(CreateUnique<CGraph>(*itr));
				if (cGraphPtr->IsValid())
				{
#ifndef NGRAPHEXECUTION
					cGraphPtr->Execute();
#endif
				}
				else
				{
					return mcFeedback = cGraphPtr->GetFeedback();
				}
			}
		}

		return mcFeedback;
	}

private:
	typedef CSmartVector<SGraphDefPtr>	CGraphDefLst;
	typedef CSmartPtr<CGraphDefLst>		CGraphDefLstPtr;
	
	CProcessorArch		mcProcessorArch;

	CFeedback			mcFeedback;
	string				msBootPath;
	string				msBootFile;

	CGraphDefLstPtr		mcGraphDefPtr;

	void init(void)
	{
		if (msBootPath.back() != cSLASH)
		{
			msBootPath += cSLASH;
		}

		mcGraphDefPtr = CreateSmart<CGraphDefLst>();
	}

	void parseCmdLineArgs(uint32_t inp_iArgCnt, char** inp_cList)
	{
		string     sWrkStrng(sNULL);
		for (uint32_t iIndx(1); iIndx < inp_iArgCnt; iIndx += 2)
		{
			sWrkStrng = string(*(++inp_cList));
			if (sWrkStrng == "-d" || sWrkStrng == "-D")
			{
				msBootFile = *(++inp_cList);
			}
			else if (sWrkStrng == "-p" || sWrkStrng == "-P")
			{
				msBootPath = *(++inp_cList);
			}
			else
			{
				break;
			}
		}
	}

	bool readBootstrap(void)
	{
		CTechPtr cTechPtr(new CSQLite());
		if (!cTechPtr->Open(msBootPath + msBootFile))
		{
			FEEDBACK(mcFeedback, 1002, sBadBootstrapOpen + msBootPath + msBootFile);
			return false;
		}
		
		SActivityPtr sActPtr(cTechPtr->Configure(sBOOTSTRAP_COMMAND, IO_DIR::INPUT, SUMMARY_FORMAT::SERIALIZED));
		if (!sActPtr)
		{
			FEEDBACK(mcFeedback, 1002, sBadBootstrapConfig + sBOOTSTRAP_COMMAND);
			return false;
		}

		SGraphDefPtr cGraphDefPtr(CreateSmart<SGraphDef>());
		sActPtr->mcContainerPtr = cGraphDefPtr.get();
		if (!cTechPtr->Receive(*sActPtr))
		{
			FEEDBACK(mcFeedback, 1003, sBadBootstrapRead + msBootPath + msBootFile);
			return false;
		}
		
		mcGraphDefPtr->push_back(cGraphDefPtr);

		return true;
	}
};
#endif
