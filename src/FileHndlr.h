#ifndef INCLUDE_H_FILEHDLR
#define INCLUDE_H_FILEHDLR

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
* (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "nodes/ITechnology.h"
#include <fstream>


static const string sBadFlatFileConfigConflict = "Incompatible I/O types for file: ";
static const string sBadFlatFileOpen = "Unable to open ";
static const string sBadFlatFileSend = "Unable to write to file: ";
static const string sBadFlatFileSendSerializedToPack = "Unable to send serialized data to packed output: ";
static const string sBadFlatFileSendPackedToSerial = "Unable to send packed data to serialized output: ";

namespace FlatFile
{
	typedef fstream				CHandle;

	typedef std::streamsize		size_type;
	typedef fstream::pos_type	pos_type;
}

struct SBufferedActivity : public SActivity
{
public:
	typedef SActivity			_Mybase;
	typedef SBufferedActivity	_Myt;

	typedef SPack::size_type	size_type;

	SPack		msBuffer;

	SBufferedActivity() :_Mybase() {}
};

class CFlatFile : public CTechnology<0>
{
public:
	typedef SBufferedActivity			packet_type;

	enum :size_t{ DEFAULT_BUFFER_SIZE = 32768 };

	CFlatFile(void) :mDirection(IO_DIR::INPUT) {}

	void Close(void)
	{
		if (mcHandle.is_open()) mcHandle.close();
	}

	SActivityPtr Configure(const string& inp_sConfig, IO_DIR inp_Direction, SUMMARY_FORMAT inp_Format)
	{
		if (!mcHandle.is_open())
		{
			mDirection = inp_Direction;
			mcHandle.open(msFilename, getOpenMode(mDirection)); 
			if (!(mcHandle.is_open()))
			{
				FEEDBACK(mcFeedback, 5010, sBadFlatFileOpen + msFilename);
				return SActivityPtr();
			}
		}
		
		if (inp_Direction != mDirection)
		{
			FEEDBACK(mcFeedback, 5010, sBadFlatFileConfigConflict + msFilename);
			return SActivityPtr();
		}

		SActivityPtr sActivityPtr(CreateSmart<packet_type>());
		sActivityPtr->mDirection = inp_Direction;
		sActivityPtr->mFormat = inp_Format;
		sActivityPtr->msCommand = inp_sConfig;

		packet_type* cPktPtr(reinterpret_cast<packet_type*>(sActivityPtr.get()));
		if (cPktPtr->mDirection == IO_DIR::INPUT)
		{
			FlatFile::pos_type iSize(mcHandle.seekg(0, mcHandle.end).tellg());

			if (iSize)
			{
				cPktPtr->msBuffer.reserve(iSize);
				return mcHandle.seekg(0, mcHandle.beg).read(cPktPtr->msBuffer.position(), iSize).good() ? sActivityPtr : SActivityPtr();
			}
			else { return sActivityPtr; }
		}
		else
		{
			cPktPtr->msBuffer.reserve(DEFAULT_BUFFER_SIZE);
			return sActivityPtr;
		}
	}

	bool Open(const string& inp_sConfig)
	{
		msFilename = inp_sConfig;
		return true;
	}

	bool Receive(SActivity& inp_cActivity)
	{
		packet_type* cPktPtr(reinterpret_cast<packet_type*>(&inp_cActivity));

		if (cPktPtr->msBuffer.miPosition >= cPktPtr->msBuffer.capacity()) return false;

		CUniquePtr<SPack>	pPtr;
		CUniquePtr<SSerial>	sPtr;

		char* cTmp;
		switch (cPktPtr->mFormat)
		{
		case SUMMARY_FORMAT::PACKED:
			pPtr = CreateUnique<SPack>();
			pPtr->mBuffer = cPktPtr->msBuffer.position();
			pPtr->miCapacity = *reinterpret_cast<SPack::size_type*>(cPktPtr->msBuffer.position() + sizeof(TYPELIST_t));
			cPktPtr->mcContainerPtr->Unpack(*pPtr);
			cPktPtr->msBuffer.miPosition += pPtr->miCapacity;
			break;
		default:
			sPtr = CreateUnique<SSerial>();
			cTmp = std::strchr(cPktPtr->msBuffer.position(), cEOL);
			sPtr->msBuffer.assign(cPktPtr->msBuffer.position(), (cTmp == nullptr ? std::strlen(cPktPtr->msBuffer.position()) : cTmp - cPktPtr->msBuffer.position()));
			cPktPtr->mcContainerPtr->Deserialize(*sPtr);
			cPktPtr->msBuffer.miPosition += sPtr->msBuffer.size() + 1;
			break;
		}

		return true;
	}

	bool Send(SActivity& inp_cActivity)
	{
		packet_type* cPktPtr(reinterpret_cast<packet_type*>(&inp_cActivity));

		CUniquePtr<SPack>	pPtr;
		CUniquePtr<SSerial>	sPtr;

		switch (cPktPtr->mFormat)
		{
		case SUMMARY_FORMAT::PACKED:
			pPtr = CreateUnique<SPack>(cPktPtr->mcContainerPtr->PackSize());
			cPktPtr->mcContainerPtr->Pack(*pPtr);
			mcHandle.write(pPtr->position(), pPtr->size()).flush();
			break;
		default:
			sPtr = CreateUnique<SSerial>();
			cPktPtr->mcContainerPtr->Serialize(*sPtr);
			mcHandle.write(sPtr->msBuffer.c_str(), sPtr->size());
			mcHandle.write(&cEOL, 1);
			mcHandle.flush();
			break;
		}

		mcHandle.flush();
		if (!isGood())
		{
			FEEDBACK(inp_cActivity.mcFeedback, 5030, sBadFlatFileSend + to_string(cPktPtr->mcContainerPtr->GetType()));
			return false;
		}

		return true;
	}

private:
	FlatFile::CHandle	mcHandle;
	string		msFilename;
	IO_DIR		mDirection;

	static ios::openmode getOpenMode(IO_DIR inp_Direction)
	{
		switch (inp_Direction)
		{
		case IO_DIR::INPUT:
			return (ios::binary | ios::in);
			break;
		default:
			return (ios::binary | ios::out);
			break;
		}
	}

	bool isEnd(void) const { return mcHandle.eof(); }
	bool isGood(void) const { return mcHandle.good(); }
};
#endif