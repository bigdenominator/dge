#ifndef INCLUDE_H_SQLITEHDLR
#define INCLUDE_H_SQLITEHDLR

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
* (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "bdsdk.h"

#include <deque>
#include "sqlite3.h"

namespace SQLite
{
	typedef	sqlite3			CHandle;			
	typedef	CHandle*		CHandlelPtr;
	typedef deque<string>	CResultLst;

	static int callback(void* io_Data, int inp_iNumCol, char** argv, char** azColName)
	{
		int iReturnStatus(0);

		if (inp_iNumCol > 0)
		{
			CResultLst*	vBuffers((CResultLst*)io_Data);

			string sWorking(argv[0]);
			for (int iCol(1); iCol < inp_iNumCol; iCol++)
			{
				sWorking += ",";
				sWorking += argv[iCol];
			}

			vBuffers->push_back(sWorking);
		}

		return iReturnStatus;
	}
}

struct SBufferLstActivity : public SActivity
{
public:
	typedef SActivity			_Mybase;
	typedef SBufferLstActivity	_Myt;

	typedef SQLite::CResultLst	result_type;

	result_type		mvResults;

	SBufferLstActivity(void) :_Mybase() {}
};

class CSQLite : public CTechnology<1>
{
public:
	typedef SBufferLstActivity		packet_type;

	CSQLite(void) {}

	void Close(void)
	{
		if (sqlite3_close(mcHandlePtr))
		{
			FEEDBACK(mcFeedback, 5002, sqlite3_errmsg(mcHandlePtr));
		}
	}

	SActivityPtr Configure(const string& inp_sConfig, IO_DIR inp_Direction, SUMMARY_FORMAT inp_Format)
	{
		SActivityPtr sActivityPtr(CreateSmart<packet_type>());
		sActivityPtr->mDirection = inp_Direction;
		sActivityPtr->mFormat = inp_Format;
		sActivityPtr->msCommand = inp_sConfig;
		
		if (sActivityPtr->mDirection == IO_DIR::INPUT)
		{
			packet_type* cPktPtr(reinterpret_cast<packet_type*>(sActivityPtr.get()));
			if (sqlite3_exec(mcHandlePtr, sActivityPtr->msCommand.c_str(), &SQLite::callback, &cPktPtr->mvResults, NULL))
			{
				FEEDBACK(mcFeedback, 5001, sqlite3_errmsg(mcHandlePtr));
				return SActivityPtr();
			}
		}
		
		return sActivityPtr;
	}

	bool Open(const string& inp_sConfig)
	{
		if (sqlite3_open(inp_sConfig.c_str(), &(mcHandlePtr)))
		{
			FEEDBACK(mcFeedback, 5003, sqlite3_errmsg(mcHandlePtr));
		}
		return mcFeedback;
	}
	
	bool Receive(SActivity& inp_cActivity)
	{
		packet_type* cPktPtr(reinterpret_cast<packet_type*>(&inp_cActivity));
		if (cPktPtr->mvResults.empty()) return false;

		CUniquePtr<SPack>	pPtr;
		CUniquePtr<SSerial>	sPtr;

		switch (cPktPtr->mFormat)
		{
		case SUMMARY_FORMAT::PACKED:
			pPtr = CreateUnique<SPack>();
			pPtr->mBuffer = const_cast<char*>(cPktPtr->mvResults.front().c_str());
			pPtr->miCapacity = cPktPtr->mvResults.front().size();
			cPktPtr->mcContainerPtr->Unpack(*pPtr);
			break;
		default:
			sPtr = CreateUnique<SSerial>();
			sPtr->msBuffer = cPktPtr->mvResults.front();
			cPktPtr->mcContainerPtr->Deserialize(*sPtr);
			break;
		}
		cPktPtr->mvResults.pop_front();

		return true;
	}

	bool Send(SActivity& inp_cActivity)
	{
		packet_type* cPktPtr(reinterpret_cast<packet_type*>(&inp_cActivity));
		if (sqlite3_exec(mcHandlePtr, inp_cActivity.msCommand.c_str(), &SQLite::callback, &cPktPtr->mvResults, NULL))
		{
			FEEDBACK(inp_cActivity.mcFeedback, 5001, sqlite3_errmsg(mcHandlePtr));
			return false;
		}
		return true;
	}

private:
	SQLite::CHandlelPtr	mcHandlePtr;
};
#endif