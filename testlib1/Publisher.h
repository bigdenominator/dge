#ifndef INCLUDE_H_PUBLISHER
#define INCLUDE_H_PUBLISHER

/* (C) Copyright Big Denominator, LLC 2016.
* Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
* (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "nodes/Library.h"
#include "Node1.h"
#include "Node2.h"

PUBLISH(1001, (PNode1, PNode2), ());
#endif