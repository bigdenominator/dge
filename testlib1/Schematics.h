#ifndef INCLUDE_H_SCHEMATICS
#define INCLUDE_H_SCHEMATICS

/* (C) Copyright Big Denominator, LLC 2016.
* Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
* (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "containers/Schematic.h"


enum TYPELIST :TYPELIST_t {
	UNAFFILIATED = FRM_TYPELIST::FRM_SCHEMATICEND,
	SNODE1,
	SNODE2, 
	SNODE1CONFIG,
	SRESULT
};

SCHEMATIC(SNode1, TYPELIST::SNODE1, (CVectorDbl::size_type miInput; CVectorDbl::size_type miOutput;), (miInput, miOutput))
SCHEMATIC(SNode2, TYPELIST::SNODE2, (double mdValue; CVectorDblPtr mcDoublesPtr;), (mdValue, mcDoublesPtr))
SCHEMATIC(SModelResult, TYPELIST::SRESULT, (double mdValue;), (mdValue))

SCHEMATIC(SNode1Config, TYPELIST::SNODE1CONFIG, (CVectorDbl::size_type miMultiplier;), (miMultiplier))
#endif