#ifndef INCLUDE_H_NODE1
#define INCLUDE_H_NODE1

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "bdsdk.h"

#include "Schematics.h"

DEF_NODE(PNode1, SNode1, (), (), (SNode1Config))
{
public:
	CVectorDbl::size_type miMultiplier;

	bool Initialize(void) 
	{
		SNode1ConfigPtr sConfigPtr;
		if (!View(sConfigPtr)) return false;

		return Initialize(sConfigPtr); 
	}

	bool Initialize(SNode1ConfigPtr inp_sConfigPtr)
	{
		miMultiplier = inp_sConfigPtr->miMultiplier;

		return true;
	}

	PROCESS_STATUS Process(SSchematicPtr inp_sSchematicPtr)
	{
		inp_sSchematicPtr->miOutput = miMultiplier * inp_sSchematicPtr->miInput;

		return PROCESS_STATUS::DONE;
	}
};
#endif
