#ifndef INCLUDE_H_NODE2
#define INCLUDE_H_NODE2

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "bdsdk.h"

#include "Schematics.h"


DEF_NODE(PNode2, SNode2, (SNode1), (SNode1Config, SModelResult), ())
{
public:
	enum :CVectorDbl::size_type { MULTIPLIER = 10 };

	bool Initialize(void) 
	{
		SNode1ConfigPtr sConfigPtr(CreateSmart<SNode1Config>());
		sConfigPtr->miMultiplier = MULTIPLIER;

		return Send(sConfigPtr); 
	}

	PROCESS_STATUS Process(SSchematicPtr inp_sSchematicPtr)
	{
		SNode1Ptr sNode1Ptr(CreateSmart<SNode1>());
		sNode1Ptr->miInput = inp_sSchematicPtr->mcDoublesPtr->size();

		return Request(sNode1Ptr, &PNode2::otherStuff, this, inp_sSchematicPtr);
	}

private:
	PROCESS_STATUS otherStuff(SSchematicPtr inp_sSchematicPtr)
	{
		*inp_sSchematicPtr->mcDoublesPtr = inp_sSchematicPtr->mdValue;

		SModelResultPtr sResultPtr(CreateSmart<SModelResult>());
		sResultPtr->mdValue = std::sum(*inp_sSchematicPtr->mcDoublesPtr);

		if (Send(sResultPtr)) return PROCESS_STATUS::DONE;
		else return PROCESS_STATUS::FAIL;

		return PROCESS_STATUS::DONE;
	}
};
#endif
