#ifndef INCLUDE_H_TEXTFILETESTS
#define INCLUDE_H_TEXTFILETESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "nodes/TransformNode.h"
#include "FileHndlr.h"

TEST(FlatFileTests, ReadWrite)
{
	string sPath("../../../unittest/data/");
	string sName("UnitTest.txt");
	string sPrefix("Line #");

	enum :int { LINES = 100};
	int iLine;

	CFlatFile ff;
	EXPECT_TRUE(ff.Open(sPath + sName));

	SActivityPtr sActSendPtr(ff.Configure("dummy", IO_DIR::OUTPUT, SUMMARY_FORMAT::SERIALIZED));
	EXPECT_TRUE((bool)sActSendPtr);

	SLogPtr sLogPtr(CreateSmart<SLog>());

	sActSendPtr->mcContainerPtr = reinterpret_cast<CContainerIntrPtr>(sLogPtr.get());
	for (iLine = 0; iLine < LINES; iLine++)
	{
		sLogPtr->msMessage = sPrefix + to_string(iLine);
		EXPECT_TRUE(ff.Send(*sActSendPtr));
	}
	ff.Close();

	iLine = 0;
	EXPECT_TRUE(ff.Open(sPath + sName));

	SActivityPtr sActReceivePtr(ff.Configure("dummy", IO_DIR::INPUT, SUMMARY_FORMAT::SERIALIZED));
	EXPECT_TRUE((bool)sActReceivePtr);

	sActReceivePtr->mcContainerPtr = reinterpret_cast<CContainerIntrPtr>(sLogPtr.get());
	bool bMore(true);
	while (bMore)
	{
		if (ff.Receive(*sActReceivePtr))
		{
			EXPECT_EQ(sPrefix + to_string(iLine++), sLogPtr->msMessage);
		}
		else
		{
			bMore = false;
		}
	}
	EXPECT_EQ(LINES, iLine);
	ff.Close();
};
#endif