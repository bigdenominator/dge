#ifndef INCLUDE_H_EVNTHNDLRTESTS
#define INCLUDE_H_EVNTHNDLRTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include <thread>
#include "EvntHndlr.h"

SCHEMATIC(STestCase, FRM_TYPELIST::STESTCASE, (uint64_t miInt64;), (miInt64))

class CEvntHndlrTests : public testing::Test
{
public:
	typedef vector<std::thread>		CThreadLst;

	atomic<uint64_t> miProducerCnt;
	atomic<uint64_t> miConsumerCnt;
	atomic<uint64_t> miProducerSum;
	atomic<uint64_t> miConsumerSum;

	uint64_t miIterations;
	uint64_t miProducerThreadCnt;
	uint64_t miConsumerThreadCnt;

	CThreadLst mvProducers;
	CThreadLst mvConsumers;

	CUniquePtr<CEvntHndlr>	mcEvntHndlrPtr;

	atomic<uint64_t>    miTestTransID;

	void SetUp(void)
	{
		mcEvntHndlrPtr = CreateUnique<CEvntHndlr>();

		CTypeLst cLst;
		cLst.insert(STestCase::TypeId);

		mcEvntHndlrPtr->configure(cLst);
	}

	void TearDown(void){}

	bool allOnReceive(void)
	{
		return !(mcEvntHndlrPtr->count(STestCase::TypeId) > -static_cast<int64_t>(miConsumerThreadCnt));
	}

	void shutdownGate1(void)
	{
		for (std::thread& cThread : mvProducers) cThread.join();
	}

	void shutdownGate2(void)
	{
		bool bAllOnReceive(false);
		while (!bAllOnReceive)
		{
			while (!allOnReceive()) {}
			bAllOnReceive = allOnReceive();
		}
	}

	void Producer(void)
	{
		const uint64_t iLocalIntrCnt(miIterations / miProducerThreadCnt);
		uint64_t iLocalSum(0);

		for (uint64_t i(0); i != iLocalIntrCnt; ++i)
		{
			STestCasePtr sSchematicPtr(CreateSmart<STestCase>());
			sSchematicPtr->miInt64 = ++miProducerCnt;

			STransportPtr   sValuePtr(mcEvntHndlrPtr->GetTransport());
			sValuePtr->msDataHldrPtr = CreateUnique<SGreyBox<STestCase>>(sSchematicPtr); 

			mcEvntHndlrPtr->Send(sValuePtr);
			iLocalSum += sSchematicPtr->miInt64;
		}
		miProducerSum += iLocalSum;
	}

	void Consumer(void)
	{
		uint64_t iLocalSum(0);
		
		STransportPtr sTmpPtr;
		while (sTmpPtr = mcEvntHndlrPtr->Receive(STestCase::TypeId))
		{
			STestCasePtr sIncomingPtr(SGreyBox<STestCase>::Extract(sTmpPtr->msDataHldrPtr));
			iLocalSum += sIncomingPtr->miInt64;
			++miConsumerCnt;

			mcEvntHndlrPtr->ReturnTransport(sTmpPtr);
		}
		miConsumerSum += iLocalSum;
	}

	void Requestor(void)
	{
		const uint64_t iLocalIntrCnt(miIterations / miProducerThreadCnt);
		uint64_t iLocalSum(0); 
		
		STestCasePtr sSchematicPtr(CreateSmart<STestCase>());
		sSchematicPtr->miInt64 = 0;

		STransportPtr   sValuePtr(mcEvntHndlrPtr->GetTransport());
		sValuePtr->msDataHldrPtr = CreateUnique<SGreyBox<STestCase>>(sSchematicPtr);
		
		for (uint64_t i(0); i < iLocalIntrCnt; i++)
		{
			sValuePtr->msReturnInfo.mbRequestWaiting = true;
			mcEvntHndlrPtr->Request(sValuePtr);

			iLocalSum += sSchematicPtr->miInt64;
			miProducerCnt++;
		}
		mcEvntHndlrPtr->ReturnTransport(sValuePtr);

		miProducerSum += iLocalSum;
	}

	void Responder(void)
	{
		uint64_t iLocalSum(0);

		STransportPtr sTmpPtr;
		while (sTmpPtr = mcEvntHndlrPtr->Receive(STestCase::TypeId))
		{
			STestCasePtr sSchematicPtr(SGreyBox<STestCase>::Extract(sTmpPtr->msDataHldrPtr));
			sSchematicPtr->miInt64++;
			iLocalSum += sSchematicPtr->miInt64;

			sTmpPtr->msReturnInfo.mbRequestWaiting = false;
			miConsumerCnt++;
		}
		miConsumerSum += iLocalSum;
	}

	void Injector(void)
	{
		STestCasePtr sSchematicPtr(CreateSmart<STestCase>());
		sSchematicPtr->miInt64 = 0;

		STransportPtr   sValuePtr(mcEvntHndlrPtr->GetTransport());
		sValuePtr->msDataHldrPtr = CreateUnique<SGreyBox<STestCase>>(sSchematicPtr);

		mcEvntHndlrPtr->Send(sValuePtr);

		miProducerCnt++;
		miProducerSum += miIterations / miConsumerThreadCnt;
	}

	void Actor(void)
	{
		const uint64_t iLocalIntrCnt(miIterations / miProducerThreadCnt);
		uint64_t iLocalSum(0);

		STransportPtr sTmpPtr;
		while (sTmpPtr = mcEvntHndlrPtr->Receive(STestCase::TypeId))
		{
			STestCasePtr sSchematicPtr(SGreyBox<STestCase>::Extract(sTmpPtr->msDataHldrPtr));

			if (sSchematicPtr->miInt64 == 0) { miConsumerCnt++; }

			if (sSchematicPtr->miInt64 < iLocalIntrCnt)
			{
				sSchematicPtr->miInt64++;
				mcEvntHndlrPtr->Send(sTmpPtr);
			}
			else
			{
				iLocalSum += sSchematicPtr->miInt64;
				mcEvntHndlrPtr->ReturnTransport(sTmpPtr);
			}
		}

		miConsumerSum += iLocalSum;
	}
};

TEST_F(CEvntHndlrTests, EvntHndlrInterfaceTest)
{
	using namespace std;
	miProducerCnt = 0;
	miConsumerCnt = 0;
	miProducerSum = 0;
	miConsumerSum = 0;

	miIterations = 2000000;
	miProducerThreadCnt = 4;
	miConsumerThreadCnt = 4;

	for (uint64_t i(0); i < miProducerThreadCnt; i++) { mvProducers.emplace_back(std::thread(&CEvntHndlrTests::Producer, this)); }
	for (uint64_t i(0); i < miConsumerThreadCnt; i++) { mvConsumers.emplace_back(std::thread(&CEvntHndlrTests::Consumer, this)); }

	shutdownGate1();
	shutdownGate2();
	mcEvntHndlrPtr->shutdown();
	for (std::thread& cThread : mvConsumers) { cThread.join(); }

	EXPECT_EQ(miProducerSum, miConsumerSum);
	EXPECT_EQ(miProducerCnt, miConsumerCnt);
};

TEST_F(CEvntHndlrTests, SyncSpeedTest)
{
	using namespace std;
	miProducerCnt = 0;
	miConsumerCnt = 0;
	miProducerSum = 0;
	miConsumerSum = 0;

	miIterations = 2000000;
	miProducerThreadCnt = 4;
	miConsumerThreadCnt = 4;

	for (uint64_t i(0); i < miProducerThreadCnt; i++) { mvProducers.emplace_back(std::thread(&CEvntHndlrTests::Requestor, this)); }
	for (uint64_t i(0); i < miConsumerThreadCnt; i++) { mvConsumers.emplace_back(std::thread(&CEvntHndlrTests::Responder, this)); }

	shutdownGate1();
	shutdownGate2();
	mcEvntHndlrPtr->shutdown();
	for (std::thread& cThread : mvConsumers) { cThread.join(); }

	EXPECT_EQ(miProducerSum, miConsumerSum);
	EXPECT_EQ(miProducerCnt, miConsumerCnt);
};
#endif
