#ifndef INCLUDE_H_BDRTCONFIGTESTS
#define INCLUDE_H_BDRTCONFIGTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "BDGraphEx.h"

TEST(BDGraphExTests, ConfigureGraph)
{
	CGraphEx cGrphEx("../../../unittest/data/", "UnitTest.db");

	CFeedback f(cGrphEx.Execute());
	EXPECT_EQ(0, f.miNum);
}
#endif
