#ifndef INCLUDE_H_BDLIBTESTS
#define INCLUDE_H_BDLIBTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "LibHndlr.h"
#include "security/BDLicense.h"
#include "EvntHndlr.h"

TEST(BDLibTests, TestLib1)
{
	SLibPkt sLib;
	sLib.msPath = "./";
	sLib.msName = "TestLib1.bd";
	sLib.msEntry = "BootstrapLib";

	CUniquePtr<CEvntHndlr> cEvntHndlrPtr(CreateUnique<CEvntHndlr>());
	SLibBootstrapPtr msBootstrapPtr(CreateSmart<SLibBootstrap>());
	msBootstrapPtr->msLicense = sNULL;
	msBootstrapPtr->mcEvntHndlrPtr = cEvntHndlrPtr.get();
	msBootstrapPtr->mcNodeSpecLstPtr = CreateSmart<CNodeSpecLst>();
	msBootstrapPtr->mcTechSpecLstPtr = CreateSmart<CTechSpecLst>();
	msBootstrapPtr->mcSchemaFnLstPtr = CreateSmart<CSchemaFnLst>();

	EXPECT_TRUE(LibHandler::open(sLib));
	EXPECT_TRUE(LibHandler::findEntry(sLib));
	EXPECT_EQ(0, sLib.mfEntry(msBootstrapPtr.get()));

	EXPECT_EQ(2, msBootstrapPtr->mcNodeSpecLstPtr->size());

	EXPECT_EQ(0, (*msBootstrapPtr->mcNodeSpecLstPtr)[1001]->mcRequestsPtr->size());
	EXPECT_EQ(2, (*msBootstrapPtr->mcNodeSpecLstPtr)[1001]->mcSendsPtr->size());
	EXPECT_EQ(1, (*msBootstrapPtr->mcNodeSpecLstPtr)[1001]->mcViewsPtr->size());

	EXPECT_EQ(1, (*msBootstrapPtr->mcNodeSpecLstPtr)[1002]->mcRequestsPtr->size());
	EXPECT_EQ(4, (*msBootstrapPtr->mcNodeSpecLstPtr)[1002]->mcSendsPtr->size());
	EXPECT_EQ(0, (*msBootstrapPtr->mcNodeSpecLstPtr)[1002]->mcViewsPtr->size());

	msBootstrapPtr.reset();
	LibHandler::close(sLib);
}
#endif