#ifndef INCLUDE_H_BDGRAPHTESTS
#define INCLUDE_H_BDGRAPHTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "BDGraph.h"

class BDGraphTests : public testing::Test
{
public:
	SGraphDefPtr msDefPtr;

	void addLibs(void)
	{
		SLibPktPtr sLibPtr(CreateSmart<SLibPkt>());
		sLibPtr->msPath = "./";
		sLibPtr->msName = "TestLib1.bd";
		sLibPtr->msEntry = "BootstrapLib";
		sLibPtr->msLicense = "3C7F2B86DBDA7759683806BF09FC58D8A5A585CBD20CC9D61812A253A5D8A828EE813EAD";

		msDefPtr->mcLibLstPtr = CreateSmart<CLibLst>();
		msDefPtr->mcLibLstPtr->add(1, sLibPtr);
	}

	void addNodeCounts(void)
	{
		msDefPtr->mcNodeCntLstPtr = CreateSmart<CNodeCntLst>();
		msDefPtr->mcNodeCntLstPtr->add(1001, 1);
		msDefPtr->mcNodeCntLstPtr->add(1002, 2);
	}

	void addExternals(void)
	{
		SActivitySpecPtr sInPtr(CreateSmart<SActivitySpec>());
		sInPtr->mDirection = IO_DIR::INPUT;
		sInPtr->mFormat = SUMMARY_FORMAT::SERIALIZED;
		sInPtr->msActivityString = "SELECT * FROM Table1";

		SActivitySpecPtr sOutPtr(CreateSmart<SActivitySpec>());
		sOutPtr->mDirection = IO_DIR::OUTPUT;
		sOutPtr->mFormat = SUMMARY_FORMAT::SERIALIZED;
		sOutPtr->msActivityString = "dummy";

		SConnSpecPtr sConnSpecPtr(CreateSmart<SConnSpec>());
		sConnSpecPtr->msConnectionString = "../../../unittest/data/testlib.db";
		sConnSpecPtr->mTechID = CSQLite::TechId;
		sConnSpecPtr->mcActivitiesPtr = CreateSmart<CActivitySpecLst>();
		sConnSpecPtr->mcActivitiesPtr->add(1002, sInPtr);
		sConnSpecPtr->mcActivitiesPtr->add(1004, sOutPtr);

		msDefPtr->mcConnSpecLstPtr = CreateSmart<CConnSpecLst>();
		msDefPtr->mcConnSpecLstPtr->push_back(sConnSpecPtr);
	}

	void SetUp(void)
	{
		msDefPtr = CreateSmart<SGraphDef>();
		msDefPtr->msLicense = "B18365BD894DFD69D3D88831F23E19B596D49B52E54EBCECF87ACE840AA13FCCAE702950";
		
		addLibs();
		addNodeCounts();
		addExternals();
	}

	void TearDown(void) {}
};

TEST_F(BDGraphTests, Configure)
{
	CGraph cTest(msDefPtr);
	EXPECT_TRUE(cTest.IsValid());
	EXPECT_EQ(0, cTest.GetFeedback().miNum);
}
#endif
