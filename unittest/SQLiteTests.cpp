#ifndef INCLUDE_H_EXTRNCNCTNTESTS
#define INCLUDE_H_EXTRNCNCTNTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "SQLiteHndlr.h"

SCHEMATIC(SPerson, 99, (string msFirst; string msLast; uint32_t miYear;), (msFirst, msLast, miYear))

static inline SPersonPtr MakePerson(string inp_sFirst, string inp_sLast, uint32_t inp_iYear)
{
	SPersonPtr sPersonPtr(CreateSmart<SPerson>());
	sPersonPtr->msFirst = inp_sFirst;
	sPersonPtr->msLast = inp_sLast;
	sPersonPtr->miYear = inp_iYear;

	return sPersonPtr;
}

TEST(SQLiteTests, ReadWrite)
{
	typedef CSmartMap<int, string>	StringLst;
	typedef CSmartPtr<StringLst>	StringLstPtr;

	typedef CSmartVector<SPersonPtr>	PersonLst;
	typedef CSmartPtr<PersonLst>		PersonLstPtr;

	const string sDbFile = sDatabasePath +"SQLiteTest.DB";
	const string sTable1("Table1");
	const string sInsertEntry("INSERT INTO " + sTable1 + "(ID, FIRST_NAME, LAST_NAME, YEAR) VALUES ");

	const string sSelect("SELECT 99,FIRST_NAME,LAST_NAME,YEAR FROM " + sTable1 + " ORDER BY ID");

	StringLstPtr	cCmtLstPtr(CreateSmart<StringLst>());
	int iCmd(0);
	cCmtLstPtr->add(iCmd++, "DROP TABLE IF EXISTS " + sTable1);
	cCmtLstPtr->add(iCmd++, "CREATE TABLE " + sTable1 + " (ID INTEGER PRIMARY KEY, FIRST_NAME TEXT, LAST_NAME TEXT, YEAR INT)");
	cCmtLstPtr->add(iCmd++, sInsertEntry + "(6, \"HENRI\", \"ROUSSEAU\", 1844)");
	cCmtLstPtr->add(iCmd++, sInsertEntry + "(5, \"EA\", \"POE\", 1850)");
	cCmtLstPtr->add(iCmd++, sInsertEntry + "(2, \"BUSTER\", \"KEATON\", 1916)");
	cCmtLstPtr->add(iCmd++, sInsertEntry + "(3, \"KARL\", \"MARX\", 1850)");
	cCmtLstPtr->add(iCmd++, sInsertEntry + "(1, \"ALBERT\", \"CAMUS\", 1850)");
	cCmtLstPtr->add(iCmd++, sInsertEntry + "(4, \"ROBERT\", \"OPPENHIMER\", 1910)");

	PersonLstPtr  cExpectedLstPtr(CreateSmart<PersonLst>());
	cExpectedLstPtr->push_back(MakePerson("ALBERT", "CAMUS", 1850));
	cExpectedLstPtr->push_back(MakePerson("BUSTER", "KEATON", 1916));
	cExpectedLstPtr->push_back(MakePerson("KARL", "MARX", 1850));
	cExpectedLstPtr->push_back(MakePerson("ROBERT", "OPPENHIMER", 1910));
	cExpectedLstPtr->push_back(MakePerson("EA", "POE", 1850));
	cExpectedLstPtr->push_back(MakePerson("HENRI", "ROUSSEAU", 1844));

	bool bResult(false);

	CSQLite cSql;
	EXPECT_TRUE(bResult = cSql.Open(sDatabaseFile));
	if (bResult)
	{
		SActivityPtr sActSendPtr(cSql.Configure("dummy", IO_DIR::OUTPUT, SUMMARY_FORMAT::SERIALIZED));
		EXPECT_TRUE(bResult = (bool)sActSendPtr);
		if (bResult)
		{
			auto itrCmd(iterators::GetItr(cCmtLstPtr));
			LOOP(itrCmd)
			{
				sActSendPtr->msCommand = itrCmd.value();
				EXPECT_TRUE(bResult = cSql.Send(*sActSendPtr));
				if (!bResult) break;
			}
		}

		SActivityPtr sActReceivePtr(cSql.Configure(sSelect, IO_DIR::INPUT, SUMMARY_FORMAT::SERIALIZED));
		EXPECT_TRUE(bResult = (bool)sActReceivePtr);
		if (bResult)
		{
			SPerson sPerson;
			sActReceivePtr->mcContainerPtr = reinterpret_cast<CContainerIntrPtr>(&sPerson);

			auto itrExp(iterators::GetItr(cExpectedLstPtr));
			LOOP(itrExp)
			{
				EXPECT_TRUE(cSql.Receive(*sActReceivePtr));
				EXPECT_EQ(itrExp.value()->msFirst, sPerson.msFirst);
				EXPECT_EQ(itrExp.value()->msLast, sPerson.msLast);
				EXPECT_EQ(itrExp.value()->miYear, sPerson.miYear);
			}
			EXPECT_FALSE(cSql.Receive(*sActReceivePtr));
		}

		cSql.Close();
	}
};
#endif
