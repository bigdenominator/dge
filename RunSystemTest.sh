
THIS_DIR=`pwd`
EXT=bin/x64/release

cd ../financialmodel/${EXT}
date +"%T"
${THIS_DIR}/${EXT}/BigDenominator -p ../../../data -d bootstrap.db
date +"%T"
cd ${THIS_DIR}
