#ifndef INCLUDE_H_BIGDENOMINATOREXE
#define INCLUDE_H_BIGDENOMINATOREXE

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"
#ifndef VERSION
#define VERSION "1.00 Development" 
#endif

#ifdef WINDOWS
#ifdef _LEAKCHK
#define LEAKCHK
#endif
#include <process.h>
#else
#define UNIX true
#define int64_t long long
#endif

using namespace std;


#define TOSTREAM ostringstream

#define ARRAY_DELETE
#endif
