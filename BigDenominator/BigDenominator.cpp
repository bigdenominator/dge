
/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "DGELib.h"

#ifdef _LEAKCHK
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#endif

int main(int argc, char* argv[])
{
#ifdef ASSERT_EXCEPTIONS
	try
	{
#endif
		int iResult(ExecuteGraphs(argc, argv));
#ifdef ASSERT_EXCEPTIONS
	}
	catch (CException cException){ cout << cException.GetWhat() << sSpace << cException.GetFile() << sSpace << cException.GetLine() << endl; }
	catch (exception cStandardLibException){ cout << cStandardLibException.what() << endl; }
	catch (...){ cout << "Undefined error occured" << endl; }
#endif
	return iResult;
}
